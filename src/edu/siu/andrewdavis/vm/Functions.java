/*
 * Functions.java
 * Defines utility functions for the VM
 * Created by Andrew Davis
 * Created on 4/16/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class Functions {
	//make the constructor private
	private Functions() {
		//no code needed
	}

	//function definitions
	
	/**
	 * Converts a byte to its hexadecimal representation as a string
	 *
	 * @param b the byte to convert
	 * @return the byte's hexadecimal representation as a string
	 */
	public static String byteToHex(byte b) {
		return String.format("%02X", b); //convert the byte to hex
	}

	/**
	 * Returns whether a given character is a numerical digit
	 *
	 * @param c the character to check
	 * @return whether the character is a digit
	 */
	public static boolean isDigit(char c) {
		//return whether the character is a digit
		return (c >= '0') && (c <= '9'); 
	}

	/**
	 * Returns whether a given character is a letter
	 *
	 * @param c the character to check
	 * @return whether the character is a letter
	 */
	public static boolean isAlpha(char c) {
		//get whether the character is lowercase
		boolean isLower = ((c >= 'a') && (c <= 'z'));

		//get whether the character is uppercase
		boolean isUpper = ((c >= 'A') && (c <= 'Z'));

		//and return the logical OR of the two values
		return isLower || isUpper;
	}

	/**
	 * Returns whether a given character is a hex digit
	 *
	 * @param c the character to check
	 * @return whether the character is a hex digit
	 */
	public static boolean isHexDigit(char c) {
		//get whether the character is a digit
		boolean digit = isDigit(c);

		//get whether the character is a lowercase hex letter
		boolean isLowerHex = ((c >= 'a') && (c <= 'f'));

		//get whether the character is an uppercase hex letter
		boolean isUpperHex = ((c >= 'A') && (c <= 'F'));

		//and return the logical OR of the three values
		return digit || isLowerHex || isUpperHex;
	}
}

//end of class
