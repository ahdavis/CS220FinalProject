/*
 * FlagState.java
 * Enumerates types of flag states for the VM
 * Created by Andrew Davis
 * Created on 4/13/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//enum definition
public enum FlagState {
	SET(true), //flag is set
	CLR(false); //flag is cleared

	//field
	private boolean data; //the data of the flag state

	/**
	 * Constructor for the FlagState enum
	 *
	 * @param newData the data for the flag state
	 */
	private FlagState(boolean newData) {
		this.data = newData; //init the data field
	}

	/**
	 * Returns the flag state's data
	 *
	 * @return the flag state's data
	 */
	public boolean getData() {
		return this.data; //return the data field
	}
}

//end of enum
