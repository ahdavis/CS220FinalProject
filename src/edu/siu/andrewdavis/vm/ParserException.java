/*
 * ParserException.java
 * Defines an exception that is thrown when a parser encounters an error
 * Created by Andrew Davis
 * Created on 4/18/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class ParserException extends Exception 
{
	/**
	 * First constructor for the ParserException class
	 *
	 * @param token the token that triggered the exception
	 */
	public ParserException(Token token) {
		//call the superclass constructor
		super("Bad token: " + token.toString());
	}

	/**
	 * Second constructor for the ParserException class
	 *
	 * @param token the token that triggered the exception
	 * @param throwable a Throwable instance to pass to the Exception
	 */
	public ParserException(Token token, Throwable throwable) {
		//call the superclass constructor
		super("Bad token: " + token.toString(), throwable);
	}
}

//end of class
