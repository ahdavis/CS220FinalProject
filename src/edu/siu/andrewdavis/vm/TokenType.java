/*
 * TokenType.java
 * Enumerates types of lexer tokens
 * Created by Andrew Davis
 * Created on 4/18/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//enum definition
public enum TokenType {
	DIR("DIR"), //directive
	LBL("LBL"), //label
	LBLD("LBLD"), //label definition 
	INS("INS"), //instruction
	REG("REG"), //register
	NUM("NUM"), //number
	STR("STR"), //string
	EOL("EOL"), //end of line
	SKP("SKP"); //skipped token

	//field
	private String name; //the name of the token type

	/**
	 * Constructor for the TokenType enum
	 *
	 * @param newName the name of the enum element
	 */
	private TokenType(String newName) {
		this.name = newName; //init the name field
	}

	/**
	 * Returns a string representation of the token type
	 *
	 * @return a string representation of the token type
	 */
	@Override
	public String toString() {
		return this.name; //return the name field
	}
}

//end of enum
