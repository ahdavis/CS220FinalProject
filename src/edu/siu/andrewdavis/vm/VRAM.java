/*
 * VRAM.java
 * Defines a class that represents virtual memory for the VM
 * Created by Andrew Davis
 * Created on 3/30/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//imports
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

//class definition
public final class VRAM {
	//fields
	private byte[] data; //the actual data stored in memory
	private int readPos; //the current read position in memory

	//constants
	
	//the default maximum size of memory
	private static final int DEFAULT_SIZE = 0x10000; 

	//the address of the start of program code memory
	public static final char PROGRAM_START = 0x0000;

	/**
	 * Default constructor for the VRAM class
	 */
	public VRAM() {
		this(DEFAULT_SIZE); //call the other constructor
	}

	/**
	 * One-arg constructor for the VRAM class
	 *
	 * @param size the number of bytes to allocate for memory
	 */
	public VRAM(int size) {
		this.data = new byte[size]; //allocate the memory
		this.readPos = 0; //init the read position

		//and zero the memory
		this.zero();
	}

	/**
	 * Returns the current read position in memory
	 *
	 * @return the current read position
	 */
	public int getReadPos() {
		return this.readPos; //return the read position field
	}

	/**
	 * Returns whether the memory is empty
	 *
	 * @return whether the memory is empty
	 */
	public boolean isEmpty() {
		boolean check = true; //holds whether the memory is empty

		//loop and verify the memory
		for(int i = 0; i < this.data.length; i++) {
			if(this.data[i] != (byte)0x00) { //non-zero byte
				check = false; //so clear the empty flag
			}
		}

		//return the empty flag
		return check;
	}

	/**
	 * Resets the current read position to 0
	 */
	public void resetReadPos() {
		this.readPos = 0x0000; //zero the read position
	}

	/**
	 * Sets the current read position
	 *
	 * @param newPos the new read position
	 */
	public void setReadPos(int newPos) {
		//validate the new position
		if(newPos < 0x0000) { //if the new position is negative
			newPos = 0x0000; //then zero it
		}

		if(newPos >= this.data.length) { //too large
			newPos = this.data.length - 1; //max it
		}

		//set the read position
		this.readPos = newPos;
	}

	/**
	 * Returns the value of the byte at a given memory address
	 *
	 * @param addr the address to read from
	 *
	 * @return the byte at that address
	 * 
	 * @throws AccessException if the memory address is out of range
	 */
	public byte peek(int addr) throws AccessException 
	{
		//change the read position
		this.readPos = addr;

		//verify that the address is in range
		if((this.readPos < 0x0000) || 
				(this.readPos >= this.data.length)) {
			//throw an exception
			throw new AccessException(this.readPos);
		}

		//if control reaches here, then the address is in range
		//so return the byte at the given address
		return this.data[this.readPos];
	}

	/**
	 * Sets the value of the byte at a given memory address
	 *
	 * @param addr the address to write to
	 * @param value the byte to write at the address
	 *
	 * @throws AccessException if the memory address is out of range
	 */
	public void poke(int addr, byte value) throws AccessException
	{
		//change the read position
		this.readPos = addr;

		//verify that the address is in range
		if((this.readPos < 0x0000) || 
				(this.readPos >= this.data.length)) { 
			//throw an exception
			throw new AccessException(this.readPos);
		}

		//if control reaches here, then the address is in range
		//so set the byte at the given address
		this.data[this.readPos] = value;
	}

	/**
	 * Stores a byte array into memory starting at a given
	 * memory address
	 *
	 * @param startAddr the starting address for the write
	 * @param bytes the byte array to store
	 *
	 * @throws AccessException if any address being written to
	 * is out of range
	 */
	public void storeBytes(int startAddr, 
			byte[] bytes) throws AccessException
	{
		//loop and write the bytes
		for(int i = 0; i < bytes.length; i++) {
			//call the poke method
			this.poke(startAddr + i, bytes[i]);  
		}
	}

	/**
	 * Stores a 16-bit integer into memory at a given memory address
	 * Note: In memory, the integer will take up two memory addresses.
	 *
	 * @param addr the address to store the integer at
	 * @param num the number to store in memory
	 *
	 * @throws AccessException if either address is out of range
	 */
	public void storeInt(int addr, char num) throws AccessException 
	{
		//create a byte buffer to hold the stored integer
		ByteBuffer buf = ByteBuffer.allocate(2);

		//set the byte order of the buffer
		buf.order(ByteOrder.LITTLE_ENDIAN);

		//store the integer in the buffer
		buf.putChar(num);

		//get the bytes from the buffer
		byte[] numData = buf.array();

		//and store the bytes in memory
		this.storeBytes(addr, numData);
	}

	/**
	 * Retrieves a 16-bit integer from memory
	 *
	 * @param addr the address to retrieve the integer from
	 *
	 * @return a 16-bit integer stored at the given address
	 *
	 * @throws AccessException if either address is out of range
	 */
	public char retrieveInt(int addr) throws AccessException 
	{
		//create a byte buffer to store the bytes of the integer
		ByteBuffer buf = ByteBuffer.allocate(2);
		
		//set the byte order of the buffer
		buf.order(ByteOrder.LITTLE_ENDIAN);

		//read the bytes from memory
		buf.put(this.peek(addr));
		buf.put(this.peek(addr + 1));

		//and return the integer that the bytes comprise
		return buf.getChar(0);
	}

	/**
	 * Reads bytes from memory until a terminating byte is reached
	 *
	 * @param addr the address to start reading from
	 * @param term the byte that signals the end of the read
	 *
	 * @return the bytes up to and including the terminator byte
	 *
	 * @throws AccessException if any address is out of range
	 */
	public byte[] retrieveBytes(int addr, 
			byte term) throws AccessException
	{
		//declare variables
		int offset = 0; //added to the read address
		int size = 0; //the size of the returned array

		//loop and calculate the size of the return array
		while(this.peek(addr + offset) != term) {
			size++; //increment the array size
			offset++; //increment the offset
		}

		//allocate a byte array to hold the retrieved bytes
		byte[] ret = new byte[size + 1];

		//zero the offset
		offset = 0;

		//loop and retrieve the bytes
		while(this.peek(addr + offset) != term) {
			//get each byte
			ret[offset] = this.peek(addr + offset);
			
			//and increment the offset
			offset++;
		}

		//get the terminating byte
		ret[offset] = this.peek(addr + offset);

		//increment the read position
		this.readPos++;

		//and return the byte array
		return ret;
	}

	/**
	 * Stores a string into memory at a given address
	 *
	 * @param addr the address to store the string at
	 * @param str the string to store
	 *
	 * @throws AccessException if any address is out of memory
	 */
	public void storeString(int addr, 
			String str) throws AccessException
	{
		//store the string in memory
		this.storeBytes(addr, str.getBytes());

		//and advance the read position to the next free address
		this.readPos += 2;
	}

	/**
	 * Returns a null-terminated string located at a given address
	 *
	 * @param addr the address to get the string from
	 *
	 * @return a null-terminated string located at the address
	 *
	 * @throws AccessException if any address is out of range
	 */
	public String retrieveString(int addr) throws AccessException 
	{
		//return a string using the retrieveBytes method
		return new String(this.retrieveBytes(addr, (byte)0x00));
	}

	/**
	 * Zeroes all the bytes in the VRAM
	 */
	public void zero() {
		//attempt to zero the memory
		try {
			//loop and zero the bytes
			for(int i = 0; i < this.data.length; i++) {
				this.poke(i, (byte)0x00); //zero each byte
			}
		} catch(AccessException e) {
			//handle the exception
			e.printStackTrace();
		}

		//and reset the read position
		this.resetReadPos();
	}

}

//end of class
