/*
 * Opcode.java
 * Enumerates opcodes for the VM
 * Created by Andrew Davis
 * Created on 3/6/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//enum definition
public enum Opcode {
	NOP(0x00, "NOP"), //no-op
	ADD(0x01, "ADD"), //adds two registers
	SUB(0x02, "SUB"), //subtracts one register from another
	MPY(0x03, "MPY"), //multiplies one register by another
	DIV(0x04, "DIV"), //divides one register by another
	MOD(0x05, "MOD"), //calculates one register modulus another
	INS(0x06, "INS"), //string input opcode
	OTS(0x07, "OTS"), //string output opcode
	INI(0x08, "INI"), //integer input opcode
	OTI(0x09, "OTI"), //integer output opcode
	HLT(0x0A, "HLT"), //halt program opcode
	LDI(0x0B, "LDI"), //load immediate value into a register
	MOV(0x0C, "MOV"), //load the word in one register into another
	LDA(0x0D, "LDA"), //load the word at an address into a register
	MVA(0x0E, "MVA"), //load the word at an address (in a register)
	LRF(0x0F, "LRF"), //load the word in a register into memory
	CPT(0x10, "CPT"), //copy pointer
	PSH(0x11, "PSH"), //push the value of a register onto the stack
	POP(0x12, "POP"), //pop the value on top of the stack to a register
	INC(0x13, "INC"), //increment a register
	DEC(0x14, "DEC"), //decrement a register
	ICP(0x15, "ICP"), //increment the value pointed to by a register
	DCP(0x16, "DCP"), //decrement the value pointed to by a register
	JMP(0x17, "JMP"), //unconditional jump
	CMP(0x18, "CMP"), //compare two registers and set the proper flags
	JNE(0x19, "JNE"), //jump if the equals flag is not set
	JE(0x1A, "JE"), //jump if the equals flag is set
	JLT(0x1B, "JLT"), //jump if the less-than flag is set
	JGT(0x1C, "JGT"), //jump if the greater-than flag is set
	CHK(0x1D, "CHK"), //check a register and set the proper flags
	JNZ(0x1E, "JNZ"), //jump if the zero flag is not set
	JZ(0x1F, "JZ"), //jump if the zero flag is set
	CALL(0x20, "CALL"), //subroutine call
	RET(0x21, "RET"), //return from subroutine
	SCMP(0x22, "SCMP"), //compare two strings and set the proper flags
	SLEN(0x23, "SLEN"), //put the length of a string into a register
	OREG(0x24, "OREG"), //output the raw value of a register
	IREG(0x25, "IREG"), //input a 16-bit value into a register
	HCF(0x26, "HCF"), //halt and catch fire
	EXG(0x27, "EXG"); //exchange register values

	//fields
	private byte op; //the integer code associated with the opcode
	private String mnemonic; //the mnemonic of the opcode

	//method definitions

	/**
	 * Constructor for the Opcode enum
	 *
	 * @param newCode the integer code associated with the opcode
	 * @param newMnemonic the mnemonic of the opcode
	 */
	private Opcode(int newCode, String newMnemonic) {
		this.op = (byte)newCode; //init the code field
		this.mnemonic = newMnemonic; //init the mnemonic field
	}

	/**
	 * Returns an opcode associated with an integer code
	 *
	 * @param code the integer code to get the opcode for
	 *
	 * @return the opcode associated with the integer
	 *
	 * @throws OpcodeException if the code does not reference a 
	 * valid opcode
	 */
	public static Opcode withCode(int code) throws OpcodeException {
		//make sure that the code is within the bounds
		//of the enum
		if((code < 0) || (code >= Opcode.values().length)) {
			//throw an exception
			throw new OpcodeException((byte)code);
		}

		//return the opcode associated with the integer code
		return Opcode.values()[code];
	}

	/**
	 * Returns an opcode associated with a mnemonic 
	 *
	 * @param codeMnemonic the mnemonic to get the opcode for
	 *
	 * @return the opcode associated with the mnemonic
	 *
	 * @throws OpcodeException if no opcode matches the mnemonic
	 */
	public static Opcode withName(String codeMnemonic) 
		throws OpcodeException {
		//loop and try to match the opcode name
		for(int i = 0; i < Opcode.values().length; i++) {
			if(Opcode.values()[i].mnemonic
					.equals(codeMnemonic)) {
				return Opcode.values()[i];
			}
		}

		//no match, so throw an OpcodeException
		throw new OpcodeException(codeMnemonic);
	}

	/**
	 * Returns the integer code associated with the opcode
	 *
	 * @return the integer code for the opcode instance
	 */
	public int getCode() {
		return (int)this.op; //return the integer opcode
	}

	/**
	 * Returns the mnemonic of the opcode
	 *
	 * @return the mnemonic of the opcode
	 */
	public String getMnemonic() {
		return this.mnemonic; //return the mnemonic field
	}

	/**
	 * Checks for equality with another opcode
	 *
	 * @param other the other opcode to compare against
	 */
	public boolean equals(Opcode other) {
		//compare the fields
		return (this.op == other.op) && 
			(this.mnemonic.equals(other.mnemonic));
	}
}

//end of enum
