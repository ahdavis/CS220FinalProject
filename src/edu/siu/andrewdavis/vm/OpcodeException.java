/*
 * OpcodeException.java
 * Defines an exception that is thrown on an invalid opcode
 * Created by Andrew Davis
 * Created on 3/6/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class OpcodeException extends Exception
{
	/**
	 * First constructor for the OpcodeException class 
	 *
	 * @param code the bad opcode
	 */
	public OpcodeException(byte code) {
		//call the superclass constructor
		super("Unknown opcode: " + Functions.byteToHex(code)); 
	}

	/**
	 * Second constructor for the OpcodeException class
	 *
	 * @param code the bad opcode
	 * @param throwable a Throwable instance to pass to the Exception
	 */
	public OpcodeException(byte code, Throwable throwable) {
		//call the superclass constructor
		super("Unknown opcode: " + Functions.byteToHex(code)
				, throwable);
	}

	/**
	 * Third constructor for the OpcodeException class
	 *
	 * @param name the name of the bad opcode
	 */
	public OpcodeException(String name) {
		//call the superclass constructor
		super("Unknown opcode name: " + name);
	}

	/**
	 * Fourth constructor for the OpcodeException class
	 *
	 * @param name the name of the bad opcode
	 * @param throwable a Throwable instance to pass to the Exception
	 */
	public OpcodeException(String name, Throwable throwable) {
		//call the superclass constructor
		super("Unknown opcode name: " + name, throwable);
	}
}

//end of class
