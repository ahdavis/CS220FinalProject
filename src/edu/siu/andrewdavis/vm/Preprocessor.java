/*
 * Preprocessor.java
 * Defines a class that preprocesses code and populates VRAM and a 
 * label/address map
 * Created by Andrew Davis
 * Created on 4/18/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//imports
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

//class definition
public final class Preprocessor {
	//fields
	private String path; //the path to the file being processed
	private Map<String, Character> addressTable; //the address table
	private VRAM vram; //the VRAM that will be populated with data
	private boolean hasRun; //has the preprocessor run?

	/**
	 * Constructor for the Preprocessor class
	 *
	 * @param newPath the path to the file to preprocess
	 */
	public Preprocessor(String newPath) {
		this.path = newPath; //init the path field
		//init the address table
		this.addressTable = new HashMap<String, Character>();
		this.vram = new VRAM(); //init the VRAM
		this.hasRun = false; //init the run field
	}

	//getter methods
	
	/**
	 * Returns the address table that has been populated
	 *
	 * @return the address table, or null if the preprocessor
	 * has not run yet
	 */
	public Map<String, Character> getAddressTable() {
		if(this.hasRun) { //if the preprocessor has run
			return this.addressTable; //then return the table
		} else { //preprocessor has not run
			return null;
		}
	}

	/**
	 * Returns the VRAM that has been populated
	 *
	 * @return the VRAM, or null if the preprocessor has not run yet
	 */
	public VRAM getVRAM() {
		if(this.hasRun) { //if the preprocessor has run
			return this.vram; //then return the VRAM
		} else { //preprocessor has not run
			return null;
		}
	}

	//other methods
	
	/**
	 * Preprocesses an entire source file and populates VRAM and the address table
	 *
	 * @throws ParserException if an error is found
	 */
	public void process() throws ParserException {
		if((this.firstPhase() + this.secondPhase()) == 0) { //if the process ran successfully
			this.hasRun = true; //then set the run flag
		}
	}

	/**
	 * Preprocesses the data section of a 
	 * source file and populates VRAM and the address table 
	 *
	 * @return 0 if the process executes successfully, 1 otherwise
	 *
	 * @throws ParserException if the preprocessor encounters an error
	 */
	private int firstPhase() throws ParserException {
		//declare variables
		File file = null;
		Scanner sc = null;

		//attempt to open the file
		try {
			file = new File(this.path); //open the file
			sc = new Scanner(file);
		} catch(FileNotFoundException e) {
			//handle the exception
			e.printStackTrace();
			
			//and return with an error
			return 1;
		} 
	
		sc.nextLine(); //skip the .data declaration

		//attempt to preprocess the file
		try {
			//loop and preprocess the data section of the file
			while(true) {
				//get the current line
				String line = sc.nextLine();

				//handle empty lines
				if(line.isEmpty()) {
					continue;
				}

				//create a PreLexer object to tokenize the
				//string
				Lexer lexer = new PreLexer(line);

				//create a Token from the lexer
				Token curToken = lexer.getNextToken();

				if(curToken.getValue().equals(".end")) {
					break;
				}


				//loop and process tokens
				while(curToken.getType() != TokenType.EOL) {
					//process the current token
					if(curToken.getType() == TokenType.DIR) {
						if(curToken.getValue().equals(".ds")) { //define string
							curToken = lexer.getNextToken();
							
							if(curToken.getType() != TokenType.LBLD) {
								throw new ParserException(curToken);
							}

							String label = curToken.getValue(); //the label for the string

							curToken = lexer.getNextToken();

							if(curToken.getType() != TokenType.NUM) {
								throw new ParserException(curToken);
							}

							char addr = (char)(Integer.parseInt(curToken.getValue(), 16)); //the address of the string

							curToken = lexer.getNextToken();

							if(curToken.getType() != TokenType.STR) {
								throw new ParserException(curToken);
							}

							String data = curToken.getValue(); //the string itself

							//put the label and the address into the address table
							this.addressTable.put(label, addr);

							//and load the string into memory
							this.vram.storeString(addr, data);

						} else if(curToken.getValue().equals(".di")) { //define integer
							curToken = lexer.getNextToken();

							if(curToken.getType() != TokenType.LBLD) {
								throw new ParserException(curToken);
							}

							String label = curToken.getValue(); //the label for the integer

							curToken = lexer.getNextToken();

							if(curToken.getType() != TokenType.NUM) {
								throw new ParserException(curToken);
							}

							char addr = (char)(Integer.parseInt(curToken.getValue(), 16)); //the address of the integer

							curToken = lexer.getNextToken();

							if(curToken.getType() != TokenType.NUM) {
								throw new ParserException(curToken);
							}

							char data = (char)(Integer.parseInt(curToken.getValue())); //the integer itself

							//put the label and the address into the address table
							this.addressTable.put(label, addr);

							//and load the integer into memory
							this.vram.storeInt(addr, data);
						} else if(curToken.getValue().equals(".da")) { //define address
							curToken = lexer.getNextToken();

							if(curToken.getType() != TokenType.LBLD) {
								throw new ParserException(curToken);
							}

							String label = curToken.getValue(); //the label for the address

							curToken = lexer.getNextToken();

							if(curToken.getType() != TokenType.NUM) {
								throw new ParserException(curToken);
							}

							char addr = (char)(Integer.parseInt(curToken.getValue(), 16)); //the address

							//put the label and the address into the address table
							this.addressTable.put(label, addr);
						}

						curToken = lexer.getNextToken(); //get the next token
							
					
					}
				}
			}
		} catch(LexerException e) {
			//handle the exception
			e.printStackTrace();

			//close the scanner
			sc.close();

			//and return with an error
			return 1;
		} catch(AccessException e) {
			//handle the exception
			e.printStackTrace();

			//close the scanner
			sc.close();

			//and return with an error
			return 1;
		}

		//close the scanner
		sc.close();

		//and return with no error
		return 0;
	}

	/**
	 * Preprocesses the code section of a 
	 * source file and populates the address table 
	 *
	 * @return 0 if the process executes successfully, 1 otherwise
	 *
	 * @throws ParserException if the parser encounters an error
	 */
	private int secondPhase() throws ParserException {
		//declare variables
		File file = null;
		Scanner sc = null;

		//attempt to open the file
		try {
			file = new File(this.path); //open the file
			sc = new Scanner(file); //open a scanner
		} catch(FileNotFoundException e) {
			//handle the error
			e.printStackTrace();

			//and return with an error
			return 1;
		}
		
		//loop through the file until the .code directive
		//is reached
		String line = sc.nextLine();

		while(!line.startsWith(".code")) {
			line = sc.nextLine();
		}

		//create an address variable to store the current byte of the program
		char pc = VRAM.PROGRAM_START;

		//loop and store labels in the address table
		try {
			while(true) {
				//move to the next line
				line = sc.nextLine();

				//check for an empty line
				if(line.isEmpty()) {
					continue;
				}

				//check for an end
				if(line.startsWith(".end")) {
					break;
				}

				//create a lexer to process the line
				Lexer lexer = new PreLexer(line);

				//get the token for the first word
				Token token = lexer.getNextToken();

				//check for a label
				if(token.getType() == TokenType.LBLD) {
					String label = token.getValue();
					this.addressTable.put(label, pc);
				}


				//loop until EOL is reached
				while(token.getType() != TokenType.EOL) {

					//handle the pc incrementation
					if(token.getType() != TokenType.EOL) {
						if(token.getType() == TokenType.NUM) {
							pc += 2;
						} else if(token.getType() == TokenType.LBL){
							pc += 2;
						} else if(token.getType() == TokenType.LBLD) {
							//do nothing
						} else {
							pc++;
						}
					}

					//get the next token
					token = lexer.getNextToken();
				}


			} 
		} catch(LexerException e) {
			e.printStackTrace(); //handle the exception

			sc.close(); //close the scanner

			return 1; //and return with an error
		}

		//close the scanner
		sc.close();

		//and return with no errors
		return 0;

	}
}

//end of class
