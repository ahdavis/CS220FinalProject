/*
 * ExecuteException.java
 * Defines an exception that is thrown when an invalid instruction
 * is encountered
 * Created by Andrew Davis
 * Created on 4/10/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class ExecuteException extends Exception
{
	/**
	 * Default constructor for the ExecuteException class 
	 */
	public ExecuteException() {
		super("Unknown instruction"); //call the superconstructor
	}

	/**
	 * One-arg constructor for the ExecuteException class
	 *
	 * @param throwable a Throwable instance to pass to the Exception
	 */
	public ExecuteException(Throwable throwable) {
		//call the superclass constructor
		super("Unknown instruction", throwable);
	}
}

//end of class
