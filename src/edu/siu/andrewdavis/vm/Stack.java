/*
 * Stack.java
 * Declares an interface for the Stack ADT
 * Created by Andrew Davis
 * Created on 3/5/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//interface declaration
public interface Stack<T> {
	//method declarations
	
	/**
	 * Adds an item to the top of the stack
	 *
	 * @param item the item to add to the stack
	 *
	 * @return whether the item was added successfully
	 */
	public boolean push(T item);

	/**
	 * Removes the item on the top of the stack and returns it
	 *
	 * @return the removed item
	 */
	public T pop();

	/**
	 * Returns the item on the top of the stack
	 *
	 * @return the top item on the stack
	 */
	public T peek();

	/**
	 * Returns whether the stack is empty
	 *
	 * @return whether the stack is empty
	 */
	public boolean isEmpty();

	/**
	 * Returns the number of items in the stack
	 *
	 * @return the number of items in the stack
	 */
	public int getSize();

	/**
	 * Clears the stack
	 */
	public void clear();
}

//end of interface
