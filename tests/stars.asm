.data

# define the strings for the program
.ds _prompt: 0x4444 "How many stars would you like?"
.ds _yes: 0x2222 "y"
.ds _again: 0x1111 "Would you like more stars? (y/n)"
.ds _clean: 0x5555 "          "
.ds _bye: 0x6666 "Goodbye!"
.ds _star: 0x7777 "*"
.ds _msg: 0x5656 "Here you go:"

# define a memory address to store the answer in
.da _ans: 0x0500 

.end

.code
_loop: # start of main loop
CALL _clearAns # clear the answer memory
CALL _dispPrompt # display the prompt
LDI 0x0000 A # put 0 into register A
IREG A # read in a number into register A
PSH B # save the B register
LDI _msg B # load the message string into B
OTS B # output the message string
POP B # get back B
CALL _dispStars # display the stars
CALL _dispMore # display a prompt for more stars
LDI _ans B # make B point to the answer
LDI _yes C # load the yes string into C
INS B # read in the response into B
SCMP B C # compare the strings pointed to by B and C
JE _loop # if they are equal (if B points to "y"), then restart the loop
LDI _bye B # load the goodbye message into B
OTS B # output B
HLT # and end the program

# dispPrompt subroutine
# Displays a prompt on the screen
_dispPrompt:
PSH B # save B
LDI _prompt B # load the prompt string
OTS B # output the prompt string
POP B # retrieve B
RET

# clearAns subroutine
# Clears the memory allocated for the 
# answer string
_clearAns:
PSH B # save B
LDI _clean B # load the empty string into B
LRF B _ans # copy the empty string into the answer memory
POP B # and retrieve B
RET

# dispStars subroutine
# Displays a number of stars equal to the value
# in the A register
_dispStars:
PSH B # save B
PSH A # save A
LDI _star B # load the star string into B
_starLoop: # start of star loop
OTS B # output the star
DEC A # decrement A
CHK A # check A for zero
JNZ _starLoop # if A is zero, then restart the loop
POP A # retrieve A
POP B # retrieve B
RET

# dispMore subroutine
# Displays a prompt for more stars
_dispMore:
PSH B # save B
LDI _again B # load the again prompt into B
OTS B # output the again prompt
POP B # and retrieve B
RET 

.end
