.data

# define the hello string
.ds _helloStr: 0xFFAA "Hello, World!"

.end

.code
LDI _helloStr B # load the hello string into register B
OTS B # output the string pointed to by register B
HLT # and end the program
.end 
