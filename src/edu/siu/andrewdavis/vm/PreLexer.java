/*
 * PreLexer.java
 * Defines a class that represents a preprocessor lexer
 * Created by Andrew Davis
 * Created on 4/18/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class PreLexer extends Lexer
{
	//no non-inherited fields
	
	/**
	 * Constructor for the PreLexer class
	 *
	 * @param newText the text to lex
	 */
	public PreLexer(String newText) {
		super(newText); //call the superclass constructor
	}

	/**
	 * Returns the next token consumed from the text
	 *
	 * @return the next token consumed from the text
	 * 
	 * @throws LexerException if the lexer fails
	 */
	@Override
	public Token getNextToken() throws LexerException {
		//loop and create tokens
		while(this.curChar != '\u0000') {
			if(this.curChar == ' ') { 
				this.skipWhitespace(); //skip the space
				continue; //and repeat the loop
			}

			if(this.curChar == '#') {
				this.comment(); //skip the comment
				continue; //and repeat the loop
			}

			if(Functions.isDigit(this.curChar)) { //digit
				//return an integer Token from the input
				return new Token(TokenType.NUM,
							this.integer());
			}

			if(this.curChar == '"') { //string
				//return a string Token from the input
				return new Token(TokenType.STR,
							this.string());
			}

			if(this.curChar == '_') { //label
				//get the symbol
				String sym = this.symbol();
				
				//get the last character
				char last = sym.charAt(sym.length() - 1);

				//handle it
				if(last == ':') { //label definition
					return new Token(TokenType.LBLD, sym.substring(0, sym.length() - 1));
				}

				//return a label token from the input
				return new Token(TokenType.LBL, sym);
			}

			if(this.curChar == '.') { //directive
				//return a directive token from
				//the input
				return new Token(TokenType.DIR,
							this.symbol());
			}

			if(Functions.isAlpha(this.curChar)) { //other data
				while((this.curChar != ' ') && (this.curChar != '\u0000')) {
					this.advance();
				}
				return new Token(TokenType.SKP, "");
			}

			//no match, so throw a LexerException
			throw new LexerException(this.curChar);
			
		}

		//return an EOL token
		return new Token(TokenType.EOL, "");
	}
}

//end of class
