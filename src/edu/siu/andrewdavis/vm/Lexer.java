/*
 * Lexer.java
 * Defines an abstract class that represents a lexer
 * Created by Andrew Davis
 * Created on 4/18/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public abstract class Lexer {
	//fields
	protected String text; //the text being lexed
	protected int pos; //the current position in the text
	protected char curChar; //the current character in the text

	/**
	 * Constructor for the Lexer class
	 *
	 * @param newText the text being lexed
	 */
	public Lexer(String newText) {
		this.text = newText; //init the text field
		this.pos = 0; //init the position field
		//init the current character
		this.curChar = this.text.charAt(this.pos);
	}

	/**
	 * Returns a Token consumed from the text
	 *
	 * @return the next Token consumed from the text
	 *
	 * @throws LexerException if lexing fails
	 */
	public abstract Token getNextToken() throws LexerException;

	/**
	 * Advances the lexer to the next character in the text
	 */
	protected void advance() {
		this.pos++; //increment the position

		//and handle end of text
		if(this.pos > (this.text.length() - 1)) {
			this.curChar = '\u0000'; //end of text
		} else {
			this.curChar = this.text.charAt(this.pos);
		}
	}

	/**
	 * Skips whitespace in the text
	 */
	protected void skipWhitespace() {
		//loop over the whitespace
		while((this.curChar != '\u0000') && 
				(this.curChar == ' ')) {
			this.advance();
		}
	}

	/**
	 * Returns a multidigit integer consumed from the text
	 *
	 * @return an integer consumed from the text as a String
	 */
	protected String integer() {
		//create a StringBuilder instance
		StringBuilder sb = new StringBuilder();

		//handle hex digits
		boolean isHex = false;
		if(this.curChar == '0') {
			if(this.text.charAt(this.pos + 1) == 'x') {
				isHex = true;
				this.advance();
				this.advance();
			}
		}

		//loop and assemble the integer string
		while((this.curChar != '\u0000') &&
				((Functions.isDigit(this.curChar)) ||
				 (Functions.isHexDigit(this.curChar)))) {
			sb.append(this.curChar); //add the character
			this.advance(); //advance the lexer
		}

		//and return the assembled string
		return sb.toString();
	}

	/**
	 * Returns a string consumed from the text
	 *
	 * @return a String consumed from the text
	 */
	protected String string() {
		//create a StringBuilder instance
		StringBuilder sb = new StringBuilder();

		//advance past the initial quote
		this.advance();

		//loop and assemble the string
		while((this.curChar != '\u0000') &&
				(this.curChar != '"')) {
			sb.append(this.curChar); //append the character
			this.advance(); //advance the lexer
		}

		//advance past the ending quote
		this.advance();

		//and return the consumed string
		return sb.toString();
	}

	/**
	 * Returns a symbol consumed from the text 
	 *
	 * @return a symbol consumed from the text
	 */
	protected String symbol() {
		//create a StringBuilder
		StringBuilder sb = new StringBuilder();

		//loop and assemble the symbol
		while((this.curChar != '\u0000') &&
				(this.curChar != ' ')) {
			sb.append(this.curChar); //append the character
			this.advance(); //advance the lexer
		}

		//and return the assembled symbol
		return sb.toString();
	}

	/**
	 * Processes a comment
	 */
	protected void comment() {
		//advance to the end of the line
		while(this.curChar != '\u0000') {
			this.advance();
		}
	}

}

//end of class
