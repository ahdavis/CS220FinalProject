/*
 * Main.java
 * Main code file for CS221 Final Project
 * Created by Andrew Davis
 * Created on 3/2/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//import
import java.util.Map;

//class definition
public class Main {
	/**
	 * Main entry point for the program
	 *
	 * @param args the command-line arguments to the program
	 */
	public static void main(String[] args) {
		
		//make sure that arguments were supplied
		if(args.length == 0) { //no args supplied
			//print out a usage message
			System.out.println(
				"Usage: java -jar VM.jar <source>");
		} else {
			try {
				//create a Preprocessor
				Preprocessor pp 
					= new Preprocessor(args[0]);

				//process the source
				pp.process();

				//get the address table
				Map<String, Character> table 
					= pp.getAddressTable();
			
				//get the VRAM
				VRAM vram = pp.getVRAM();

				if((table != null) && (vram != null)) {
					//create an assembler 
					//to assemble the code with
					Assembler asm 
						= new Assembler(args[0], 
							table, vram);

					//assemble the code
					if(asm.assemble() == 0) {
						//create a VM to run
						//the code with
						VM vm = 
						new VM(asm.getVRAM()); 

						//run the code
						vm.execute(); 

						//and close the VM
						vm.close(); 
					} else {
						System.out
						.println(
							"Assembly failed");
					}
				} else {
					System.out
					.println("Preprocessing failed");
				}

			} catch(ParserException e) {
				e.printStackTrace();
			} catch(AccessException e) {
				e.printStackTrace();
			} catch(ExecuteException e) {
				e.printStackTrace();
			}
		}

	}
}

//end of program
