/*
 * RegNameException.java
 * Defines an exception that is thrown when an invalid register name is given
 * Created by Andrew Davis
 * Created on 4/19/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class RegNameException extends Exception
{
	//no fields
	
	/**
	 * First constructor for the RegNameException class
	 *
	 * @param name the name that triggered the exception
	 */
	public RegNameException(String name) {
		//call the superclass constructor
		super("Bad register name: " + name);
	}

	/**
	 * Second constructor for the RegNameException class
	 *
	 * @param name the name that triggered the exception
	 * @param throwable a Throwable instance to pass to the Exception
	 */
	public RegNameException(String name, Throwable throwable) {
		//call the superclass constructor
		super("Bad register name: " + name, throwable);
	}
}

//end of class
