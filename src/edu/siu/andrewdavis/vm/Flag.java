/*
 * Flag.java
 * Defines a class that represents a processor flag for the VM
 * Created by Andrew Davis
 * Created on 4/13/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class Flag {
	//fields
	private FlagType type; //which flag is this?
	private FlagState state; //the state of the flag

	/**
	 * First constructor for the Flag class
	 *
	 * @param newType the type of the Flag
	 */
	public Flag(FlagType newType) {
		this(newType, FlagState.CLR); //call the other constructor
	}

	/**
	 * Second constructor for the Flag class
	 *
	 * @param newType the type of the flag
	 * @param newState the state of the flag
	 */
	public Flag(FlagType newType, FlagState newState) {
		this.type = newType; //init the flag type
		this.state = newState; //init the flag state
	}

	//getter methods
	
	/**
	 * Returns the type of the flag
	 *
	 * @return the type of the flag
	 */
	public FlagType getType() {
		return this.type; //return the type field
	}

	/**
	 * Returns the state of the flag
	 *
	 * @return the state of the flag
	 */
	public FlagState getState() {
		return this.state; //return the state field
	}

	//other methods
	
	/**
	 * Sets the flag
	 */
	public void set() {
		this.state = FlagState.SET; //set the flag
	}

	/**
	 * Clears the flag
	 */
	public void clear() {
		this.state = FlagState.CLR; //clear the flag
	}
}

//end of class
