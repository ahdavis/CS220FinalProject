/*
 * Token.java
 * Defines a class that represents a lexer token
 * Created by Andrew Davis
 * Created on 4/18/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class Token {
	//fields
	private TokenType type; //the type of the Token
	private String value; //the value of the Token

	/**
	 * Constructor for the Token class
	 *
	 * @param newType the type of the Token
	 * @param newValue the value of the Token
	 */
	public Token(TokenType newType, String newValue) {
		this.type = newType; //init the type field
		this.value = newValue; //init the value field
	}

	//getter methods
	
	/**
	 * Returns the type of the Token
	 *
	 * @return the type of the Token
	 */
	public TokenType getType() {
		return this.type; //return the type field
	}

	/**
	 * Returns the value of the Token
	 *
	 * @return the value of the Token
	 */
	public String getValue() {
		return this.value; //return the value field
	}

	/**
	 * Returns a string representation of the Token
	 *
	 * @return a string representation of the Token
	 */
	@Override
	public String toString() {
		return "Type: " + this.type.toString() + " Value: " +
					this.value;
	}
}

//end of class
