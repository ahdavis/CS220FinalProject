.data 

# define strings to be used in the program
.ds _info: 0xFFAA "This program calculates the Fibonacci sequence."
.ds _prompt: 0xEEEE "Enter the number of terms to display:"
.ds _msg: 0xBBBB "Here's your sequence:"
.ds _error: 0xAAAA "Can't enter less than 1 term"
.ds _resp: 0xDDDD "Here you go:"

.end

.code 
CALL _SETUP # call the setup subroutine
PSH B # save the B register because the PRMPT routine destroys it
CALL _PRMPT  # prompt for a number of terms
POP B # get back the B register
CALL _RUN  # and display the sequence
HLT

# SETUP subroutine
# Sets up the program
_SETUP:
LDI 1 B #the first term in the sequence
LDI 1 C #the second term in the sequence
LDI 0 D #the third term in the sequence
LDI 1 E #the number of terms to display
LDI 1 F #used to validate the number of terms entered
RET 

# PRMPT subroutine
# Displays the info and prompt messages
# and reads in the number of terms
_PRMPT:
LDI _info B # load the info message
OTS B # and display it
_input: # start of input validation loop
LDI _prompt B # load the prompt message
OTS B # display it
IREG E # read the number of terms into register E
CMP E F # make sure that the entered number is not less than one
JLT _badinput # if it is < 1, then jump to the bad input code
RET # return from the subroutine
_badinput: # start of bad input code
PSH B # save register B
LDI _error B # load the error message
OTS B # and display it
POP B # get back the B register
JMP _input # and restart the validation loop

# RUN subroutine
# Displays the actual sequence
_RUN:
PSH B # save the B register
LDI _resp B # load the response string
OTS B # and display it
POP B # and get back the B register
_loop: # start of sequence loop
OREG B # display the contents of the B register
PSH C # save the C register
ADD C B # add C and B and put the result in C
MOV C D # put C in D
POP C # get back the C register
MOV C B # put C in B
MOV D C # put D in C
DEC E # decrement the E register
CHK E # check the E register for 0
JNZ _loop # and repeat the loop if E is not 0
RET # return from subroutine

.end
