/*
 * AsmLexer.java
 * Defines a class that lexes assembly code
 * Created by Andrew Davis
 * Created on 4/19/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class AsmLexer extends Lexer
{
	//no non-inherited fields
	
	/**
	 * Constructor for the AsmLexer class
	 *
	 * @param newText the text to lex
	 */
	public AsmLexer(String newText) {
		super(newText); //call the superclass constructor
	}

	/**
	 * Processes the text and returns the next token
	 *
	 * @return the next token consumed from the input
	 * @throws LexerException if an invalid character is encountered
	 */
	@Override
	public Token getNextToken() throws LexerException {
		//loop and create tokens
		while(this.curChar != '\u0000') {
			if(this.curChar == ' ') { 
				this.skipWhitespace(); //skip the space
				continue; //and repeat the loop
			}

			if(this.curChar == '#') {
				this.comment(); //skip the comment
				continue; //and repeat the loop
			}

			if(Functions.isDigit(this.curChar)) { //digit
				//return an integer Token from the input
				return new Token(TokenType.NUM,
							this.integer());
			}

			if(this.curChar == '"') { //string
				//skip the string
				return new Token(TokenType.SKP,
							this.string());
			}

			if(this.curChar == '_') { //label
				//get the symbol
				String sym = this.symbol();

				//determine whether a label definition was found
				char end = sym.charAt(sym.length() - 1);
				if(end == ':') { //label definition
					return new Token(TokenType.LBLD, sym.substring(0, sym.length() - 1));
				}

				//return a label token from the input
				return new Token(TokenType.LBL, sym);
			}

			if(this.curChar == '.') { //directive
				//skip the directive
				return new Token(TokenType.SKP,
							this.symbol());
			}

			if(Functions.isAlpha(this.curChar)) { //other data
				//get the symbol
				String sym = this.symbol();

				//check the symbol for a PC register name
				if(sym.equals("PC")) {
					return new Token(TokenType.REG, sym);
				}

				//check the symbol for a regular register
				if(sym.length() == 1) {
					return new Token(TokenType.REG, sym);
				}

				//finally, check the symbol for an instruction
				return new Token(TokenType.INS, sym);
			}

			//no match, so throw a LexerException
			throw new LexerException(this.curChar);
			
		}

		//return an EOL token	
		return new Token(TokenType.EOL, "");
	}
}

//end of class
