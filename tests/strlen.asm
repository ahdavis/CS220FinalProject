.data

# define strings to be used by the program
.ds _prompt: 0xBBBB "Enter a string:"
.ds _resp: 0xAAAA "Here is your string's length:"

# define an address to store the read string at
.da _str: 0xFFAA

.end

.code 
LDI _str B # load the string memory
LDI _prompt C # load the prompt string
OTS C # output the prompt string
INS B # read in a string into the string memory
SLEN B A # put the length of the string into A
LDI _resp B # load the response string
OTS B # output the response
OREG A # and output the value of the A register
HLT

.end 
