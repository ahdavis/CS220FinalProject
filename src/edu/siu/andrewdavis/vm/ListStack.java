/*
 * ListStack.java
 * Linked list implementation of the Stack ADT
 * Created by Andrew Davis
 * Created on 3/5/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class ListStack<T> implements Stack<T>
{
	//fields
	private Node<T> firstNode; //the first node in the list
	private int nodeCount; //the number of nodes in the list

	/**
	 * Constructor for the ListStack class
	 */
	public ListStack() {
		this.firstNode = null; //zero the first node
		this.nodeCount = 0; //init the node count to 0
	}

	/**
	 * Adds an item to the top of the stack
	 *
	 * @param item the item to add
	 *
	 * @return whether the item was added successfully
	 */
	public boolean push(T item) {
		if(this.firstNode == null) { //if the stack is empty
			//then create the first node
			this.firstNode = new Node<T>(item);
		} else { //if the stack is not empty
			//then create the next node in the list
			this.firstNode = new Node<T>(item, this.firstNode);
		}

		//increment the node count
		this.nodeCount++;

		//and return true
		return true;
	}

	/**
	 * Removes the item at the top of the stack and returns it
	 *
	 * @return the removed item
	 */
	public T pop() {
		//make sure that the stack is not empty
		if(this.isEmpty()) { //if the stack is empty
			return null; //then return null
		}

		//handle a list with one entry
		if(this.nodeCount == 1) {
			//get the top item
			T ret = this.peek();

			//remove the first node
			this.firstNode = null;

			//decrement the node count
			this.nodeCount--;

			//and return the top item
			return ret;
		}

		//if control reaches here, then the
		//list has more than one item

		//get the item at the top of the stack
		T ret = this.peek();

		//remove the first node
		this.firstNode = this.firstNode.getNext();

		//decrement the node count
		this.nodeCount--;

		//and return the former top item
		return ret;
	}

	/**
	 * Returns the item on the top of the stack (does not remove it)
	 *
	 * @return the item on the top of the stack
	 */
	public T peek() {
		//return the first node's data
		return this.firstNode.getData();
	}

	/**
	 * Returns whether the stack is empty
	 *
	 * @return whether the stack is empty
	 */
	public boolean isEmpty() {
		//return whether the first node is null
		return this.firstNode == null;
	}

	/**
	 * Returns the number of items in the stack
	 *
	 * @return the number of items in the stack
	 */
	public int getSize() {
		return this.nodeCount; //return the node count
	}

	/**
	 * Clears the stack
	 */
	public void clear() {
		this.firstNode = null; //null the first node
	}

	//private Node class - defines a linked list node
	private class Node<T> {
		//fields
		private Node<T> next; //the next node in the list
		private T data; //the data for the node

		/**
		 * First constructor for the Node class
		 *
		 * @param newData the value of the new Node
		 */
		public Node(T newData) {
			this(newData, null); //call the other constructor
		}

		/**
		 * Second constructor for the Node class
		 *
		 * @param newData the value of the new Node
		 * @param newNext the new next node in the list
		 */
		public Node(T newData, Node<T> newNext) {
			this.next = newNext; //init the next node
			this.data = newData; //init the data field
		}

		/**
		 * Returns the next node in the list
		 * 
		 * @return the next node in the list
		 */
		public Node<T> getNext() {
			return this.next; //return the next node field
		}

		/**
		 * Returns the data of the current node
		 *
		 * @return the current node's data
		 */
		public T getData() {
			return this.data; //return the data field
		}
	}
	//end of Node class
}

//end of class
