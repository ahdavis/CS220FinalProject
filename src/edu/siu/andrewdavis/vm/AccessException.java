/*
 * AccessException.java
 * Defines an exception that is thrown when an invalid memory address
 * is examined
 * Created by Andrew Davis
 * Created on 3/30/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class AccessException extends Exception
{
	//no fields
	
	/**
	 * First constructor for the AccessException class
	 * @param addr the address that triggered the exception
	 */
	public AccessException(int addr) {
		//call the superclass constructor
		super("VRAM access violation! Address: " 
				+ Integer.toString(addr)); 
	}

	/**
	 * Second constructor for the AccessException class
	 *
	 * @param addr the address that triggered the exception
	 * @param throwable a Throwable instance to pass to the superclass
	 */
	public AccessException(int addr, Throwable throwable) {
		//call the superclass constructor
		super("VRAM access violation! Address: "
				+ Integer.toString(addr), throwable);
	}
}

//end of class
