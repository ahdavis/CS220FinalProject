/*
 * VM.java
 * Defines a class that represents a virtual machine
 * Created by Andrew Davis
 * Created on 4/10/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//imports
import java.util.Scanner;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

//class definition
public class VM {
	//fields
	private Stack<Character> stack; //the processor stack
	private VRAM memory; //the processor memory
	private Register[] registers; //the processor registers
	private Flag equalsFlag; //the equals flag
	private Flag zeroFlag; //the zero flag
	private Flag ltFlag; //the less-than flag
	private Flag gtFlag; //the greater-than flag
	private Scanner sc; //the input device for the VM

	/**
	 * Default constructor for the VM class
	 */
	public VM() {
		this(new VRAM()); //call the other constructor
	}

	/**
	 * Main constructor for the VM class
	 *
	 * @param newMemory the memory for the VM
	 */
	public VM(VRAM newMemory) {
		//init the fields
		this.stack = new ListStack<Character>(); //init the stack
		this.memory = newMemory; //init the memory field
		this.registers = new Register[RegName.REG_COUNT];

		//init the program counter
		this.registers[0] = new Register(RegName.PC,
						VRAM.PROGRAM_START);

		//init the other registers
		for(int i = 1; i < RegName.REG_COUNT; i++) {
			//init each register
			this.registers[i] = 
				new Register(RegName.values()[i]);
		}

		//init the flags
		this.equalsFlag = new Flag(FlagType.EQU);
		this.zeroFlag = new Flag(FlagType.ZRO);
		this.ltFlag = new Flag(FlagType.LT);
		this.gtFlag = new Flag(FlagType.GT);

		//init the scanner
		this.sc = new Scanner(System.in);
	}

	//setter method
	
	/**
	 * Sets the memory for the VM
	 *
	 * @param newMemory the new memory
	 */
	public void setMemory(VRAM newMemory) {
		this.memory = newMemory;
	}

	//other methods
	
	/**
	 * Runs code stored in memory
	 *
	 * @throws ExecuteException if an unknown instruction is found
	 * @throws AccessException if memory is accessed wrongly
	 */
	public void execute() throws ExecuteException, AccessException {
		//loop and execute code as long as a HLT
		//instruction is not encountered
		while(true) {
			//declare a variable to determine whether
			//a jump was executed
			boolean didJump = false;

			//get the current opcode
			Opcode curCode = this.fetch();

			//switch on the opcode
			switch(curCode) {
				case NOP: //NOP instruction
					{
						//do nothing
						break;
					}
				case ADD: //addition instruction
					{
						//increment the program
						//counter
						this.nextByte();

						//get the first argument 
						//register
						Register r1 = 
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())];

						//increment the program
						//counter
						this.nextByte();

						//get the value of the
						//second register
						char v1 =
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())]
						.getValue();

						//execute the addition
						r1.add(v1);

						//and assign back the
						//updated register
						this.registers[
						r1.getName().getIndex()] =
							r1;

						break;
					}
				case SUB: //subtraction instruction
					{
						//increment the program
						//counter
						this.nextByte();

						//get the first argument 
						//register
						Register r1 = 
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())];

						//increment the program
						//counter
						this.nextByte();

						//get the value of the
						//second register
						char v1 =
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())]
						.getValue();

						//execute the subtraction
						r1.sub(v1);

						//and assign back the
						//updated register
						this.registers[
						r1.getName().getIndex()] =
							r1;

						break;
					}
				case MPY: //multiplication
					{
						//increment the program
						//counter
						this.nextByte();

						//get the first argument 
						//register
						Register r1 = 
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())];

						//increment the program
						//counter
						this.nextByte();

						//get the value of the
						//second register
						char v1 =
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())]
						.getValue();

						//execute the 
						//multiplication
						r1.mpy(v1);

						//and assign back the
						//updated register
						this.registers[
						r1.getName().getIndex()] =
							r1;

						break;
					}
				case DIV: //division instruction
					{
						//increment the program
						//counter
						this.nextByte();

						//get the first argument 
						//register
						Register r1 = 
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())];

						//increment the program
						//counter
						this.nextByte();

						//get the value of the
						//second register
						char v1 =
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())]
						.getValue();

						//execute the division
						r1.div(v1);

						//and assign back the
						//updated register
						this.registers[
						r1.getName().getIndex()] =
							r1;

						break;
					}
				case MOD: //modulus instruction
					{
						//increment the program
						//counter
						this.nextByte();

						//get the first argument 
						//register
						Register r1 = 
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())];

						//increment the program
						//counter
						this.nextByte();

						//get the value of the
						//second register
						char v1 =
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())]
						.getValue();

						//execute the modulus
						r1.mod(v1);

						//and assign back the
						//updated register
						this.registers[
						r1.getName().getIndex()] =
							r1;

						break;
					}

				case INS: //string input
					{
						//get the next byte
						this.nextByte();

						//get the address to
						//store the string into
						int addr = 
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())]
						.getValue();

						String str = "";
						//read in the string
						if(this.sc.hasNextLine()) {
							str = this.sc
							.nextLine();
						}

						//and store the string
						try {
							this.memory.
							storeString(
								addr,
								str);
						} catch(AccessException e) 
						{
							e
							.printStackTrace();
						}

						break;
					}
				case OTS: //string output opcode
					{
						//get the next byte
						this.nextByte();

						//get the address to
						//retrieve the string from
						int addr =
						this.registers[
						this.memory.peek(
						this.registers[0].
							getValue())]
						.getValue();

						//output the string
						try {
						System.out.println(
							this.memory.
							retrieveString(
								addr));
						} catch(AccessException e) 
						{
							e.
							printStackTrace();
						}

						break;
							
					}
				case INI: //integer input
					{
						//get the next byte
						this.nextByte();

						//get the address to
						//store the integer into
						int addr = 
						this.registers[
						this.memory.peek(
						this.registers[0]
							.getValue())]
						.getValue();

						int i = 0;

						//read in the integer
						if(this.sc.hasNextInt()) {
							//get the next int
							i = this.sc
							.nextInt();

							//and consume the
							//newline
							this.sc.nextLine();

						}

						//and store the integer
						try {
							this.memory.
							storeInt(
								addr, 
								(char)i);
						} catch(AccessException e) 
						{
							e
							.printStackTrace();
						}


						break;
					}
				case OTI: //integer output opcode
					{
						//get the next byte
						this.nextByte();

						//get the address to
						//retrieve the int from
						char addr =
						this.registers[
						this.memory.peek(
						this.registers[0].
							getValue())]
						.getValue();

						//output the int
						try {
						System.out.println(
							this.memory.
							retrieveInt(addr));
						} catch(AccessException e) 
						{
							e.
							printStackTrace();
						}

						break;
							
					}

				case HLT: //halt instruction
					{
						return; //exit execution
					}

				case LDI: //load immediate
					{
						//get the next byte
						this.nextByte();

						//create a byte buffer
						//to retrieve the next
						//two bytes
						ByteBuffer buf =
							ByteBuffer.
							allocate(2);

						//set the byte order of the
						//buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						byte b1 = this.memory
							.peek(
							this.registers[0].
							getValue());

						//put it in the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte
						byte b2 = this.memory
							.peek(
							this.registers[0].
							getValue());

						//put it in the buffer
						buf.put(b2);

						//get the 16-bit value that
						//the two bytes make up
						char c = buf.getChar(0);

						//get the next byte
						this.nextByte();

						//get the register to
						//put the short in
						Register r = 
							this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())];

						//store the value in the
						//register
						r.setValue(c);

						//and assign back the
						//updated register
						this.registers[
							r.getName()
							.getIndex()] = r;

						break;
						
					}

				case MOV: //move registers
					{
						//get the next byte
						this.nextByte();

						//get the source register's
						//value
						char v = this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())].
							getValue();

						//get the next byte
						this.nextByte();

						//get the destination
						//register
						Register r = 
							this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())];

						//set the value of the
						//destination register
						r.setValue(v);

						//and assign back the
						//updated register
						this.registers[r.getName().
							getIndex()] = r;

						break;
					}

				case LDA: //load value at address
					{
						//get the next byte
						this.nextByte();

						//create a byte buffer
						//to retrieve the
						//address
						ByteBuffer buf =
						ByteBuffer.allocate(2);

						//set the buffer's byte
						//order
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						byte b1 = this.memory.peek(
							this.registers[0].
							getValue());

						//put it in the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte
						byte b2 = this.memory.peek(
							this.registers[0].
							getValue());

						//put it in the buffer
						buf.put(b2);

						//get the address from the
						//two bytes
						int a = buf.getChar(0);

						//get the word at the
						//address
						char c = this.memory.retrieveInt(a);
						
						//get the next byte
						this.nextByte();

						//get the register to
						//store the byte in
						Register r = 
							this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())];

						//store the byte in the
						//register
						r.setValue(c);

						//and assign back the
						//updated register
						this.registers[r.getName().
							getIndex()] = r;

						break;

					}

				case MVA: //move word at address
					{
						//get the next byte
						this.nextByte();

						//get the address from
						//the first register
						char addr = 
							this.registers[
							this.memory.peek(
							this.registers[0]
							.getValue())]
							.getValue();

						//get the word at that
						//address
						char data = this.memory.
							retrieveInt(addr);

						//get the next byte
						this.nextByte();

						//get the register to
						//load the byte into
						Register r = 
						this.registers[
						this.memory.peek(
							this.registers[0].
							getValue())];

						//load the byte into the
						//register
						r.setValue(data);

						//and assign back the
						//updated register
						this.registers[
							r.getName().
								getIndex()]
							= r;

						break;
					}

				case LRF: //load value into memory
					{
						//get the next byte
						this.nextByte();

						//get the value to
						//load into memory
						char v = this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())]
							.getValue();

						//get the next byte
						this.nextByte();

						//get a byte buffer to
						//load the address with
						ByteBuffer buf =
						ByteBuffer.allocate(2);

						//set the byte order of the
						//buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						byte b1 = this.memory.peek(
							this.registers[0].
							getValue());

						//put the first byte into
						//the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte
						byte b2 = this.memory.peek(
							this.registers[0].
							getValue());

						//put the second byte into
						//the buffer
						buf.put(b2);

						//get the address from the
						//buffer
						char addr = buf.getChar(0);

						//and put the value into
						//memory at the address
						this.memory.storeInt(v,
								addr);

						break;
					}
				
				case CPT: //copy pointer
					{
						//get the next byte
						this.nextByte();

						//get the address to load
						char addr = this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())]
							.getValue();

						//get the next byte
						this.nextByte();

						//get the register to
						//load the address into
						Register r = 
							this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())];

						//load the address into
						//the register
						r.setValue(addr);

						//and assign back the
						//updated register
						this.registers[
							r.getName().
								getIndex()]
							= r;

						break;
					}

				case PSH: //push register onto stack
					{
						//get the next byte
						this.nextByte();

						//get the value to push
						char v = this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())]
							.getValue();

						//and push it onto the 
						//stack
						this.stack.push(v);

						break;
					}
				case POP: //pop stack into register
					{
						//get the next byte
						this.nextByte();

						//get the register to pop
						//into
						Register r = 
							this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())];

						//pop the top of the stack
						//into the register
						r.setValue(this.stack
								.pop());

						//and assign back the
						//updated register
						this.registers[
							r.getName().
							getIndex()] = r;

						break;
					}
				case INC: //increment a register
					{
						//get the next byte
						this.nextByte();

						//and increment the
						//indicated register
						this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())].inc();

						break;
					}

				case DEC: //decrement a register
					{
						//get the next byte
						this.nextByte();

						//and decrement the
						//indicated register
						this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())].dec();

						break;
					}

				case ICP: //increment the value in a reg
					{
						//get the next byte
						this.nextByte();

						//get the address of the
						//value to increment
						char a = this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())]
							.getValue();

						//get the byte at that
						//address
						byte b = this.memory.peek(
							a);

						//increment it
						b++;

						//and put it back in
						//memory
						this.memory.poke(a, b);

						break;
					}
				case DCP: //decrement the value in a reg
					{
						//get the next byte
						this.nextByte();

						//get the address of the
						//value to increment
						char a = this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())]
							.getValue();

						//get the byte at that
						//address
						byte b = this.memory.peek(
							a);

						//decrement it
						b--;

						//and put it back in
						//memory
						this.memory.poke(a, b);

						break;
					}

				case JMP: //unconditional jump
					{
						//get the next byte
						this.nextByte();

						//create a byte buffer
						//to store the address
						//in
						ByteBuffer buf =
							ByteBuffer.
							allocate(2);

						//set the order of the
						//buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte of
						//the address
						byte b1 = this.memory.peek(
							this.registers[0].
							getValue());

						//put it in the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte
						//of the address
						byte b2 = this.memory.peek(
							this.registers[0].
							getValue());

						//put it in the buffer
						buf.put(b2);

						//get the address from
						//the buffer
						char addr = buf.getChar(0);

						//set the program
						//counter to the address
						this.registers[0]
							.setValue(addr);

						//and set the jump flag
						didJump = true;
								
						break;
					}
					
				case CMP: //compare registers
					{
						//get the next byte
						this.nextByte();

						//get the first register
						Register r1 = 
							this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())];


						//get the next byte
						this.nextByte();

						//get the second register
						Register r2 =
							this.registers[
							this.memory.peek(
							this.registers[0].
							getValue())];


						//compare the registers
						int res = r1.compareTo(r2);

						//and set the proper flags
						if(res == 0) { //equal
							//set the equals
							//flag
							this.equalsFlag
								.set();

							//clear the less
							//than flag
							this.ltFlag
								.clear();

							//clear the 
							//greater than
							//flag
							this.gtFlag
								.clear();
						} else if(res < 0) { //<
							//clear the equals
							//flag
							this.equalsFlag
								.clear();

							//set the less
							//than flag
							this.ltFlag.set();

							//clear the
							//greater than
							//flag
							this.gtFlag
								.clear();
						} else { //greater than
							//clear the equals
							//flag
							this.equalsFlag
								.clear();

							//clear the less
							//than flag
							this.ltFlag
								.clear();

							//set the greater
							//than flag
							this.gtFlag.set();
						}

						break;
					}

				case JNE: //jump if not equal
					{
						//get the next byte
						this.nextByte();

						//determine whether the
						//equals flag is set
						if(this.equalsFlag
							.getState()
							.getData()) {
							//flag is set
							this.nextByte();
							break;
						}

						//get a byte buffer to
						//get the address to
						//jump to
						ByteBuffer buf =
						ByteBuffer.allocate(2);

						//set the byte order
						//of the buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						//of the address
						byte b1 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte of
						//the address
						byte b2 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b2);

						//get the address from
						//the buffer
						char addr = buf.getChar(0);
						
						//set the program
						//counter to the address
						this.registers[0]
							.setValue(addr);

						//and set the jump
						//flag
						didJump = true;
						
						break;
					}
				case JE: //jump if equal
					{
						//get the next byte
						this.nextByte();

						//determine whether the
						//equals flag is set
						if(!this.equalsFlag
							.getState()
							.getData()) {
							//flag is cleared
							this.nextByte();
							break;
						}

						//get a byte buffer to
						//get the address to
						//jump to
						ByteBuffer buf =
						ByteBuffer.allocate(2);

						//set the byte order
						//of the buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						//of the address
						byte b1 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte of
						//the address
						byte b2 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b2);

						//get the address from
						//the buffer
						char addr = buf.getChar(0);
						
						//set the program
						//counter to the address
						this.registers[0]
							.setValue(addr);

						//and set the jump
						//flag
						didJump = true;
						
						break;
					}
				case JLT: //jump if less than
					{
						//get the next byte
						this.nextByte();

						//determine whether the
						//less-than flag is cleared
						if(!this.ltFlag.getState()
							.getData()) {
							//flag is cleared
							this.nextByte();
							break;
						}

						//get a byte buffer to
						//get the address to
						//jump to
						ByteBuffer buf =
						ByteBuffer.allocate(2);

						//set the byte order
						//of the buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						//of the address
						byte b1 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte of
						//the address
						byte b2 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b2);

						//get the address from
						//the buffer
						char addr = buf.getChar(0);
						
						//set the program
						//counter to the address
						this.registers[0]
							.setValue(addr);

						//and set the jump
						//flag
						didJump = true;
						
						break;
					}
				case JGT: //jump if greater than
					{
						//get the next byte
						this.nextByte();

						//determine whether the
						//greater-than flag
						//is cleared
						if(!this.gtFlag.getState()
							.getData()) {
							//flag is cleared
							this.nextByte();
							break;
						}

						//get a byte buffer to
						//get the address to
						//jump to
						ByteBuffer buf =
						ByteBuffer.allocate(2);

						//set the byte order
						//of the buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						//of the address
						byte b1 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte of
						//the address
						byte b2 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b2);

						//get the address from
						//the buffer
						char addr = buf.getChar(0);
						
						//set the program
						//counter to the address
						this.registers[0]
							.setValue(addr);

						//and set the jump
						//flag
						didJump = true;
						
						break;
					}
				case CHK: //check single register
					{
						//get the next byte
						this.nextByte();

						//get the register to
						//check
						Register r = 
						this.registers[
						this.memory.peek(
						this.registers[0].
						getValue())];

						//and check it
						if(r.getValue() == 0) {
							//set the zero
							//flag
							this.zeroFlag
								.set();
						} else {
							//clear the zero
							//flag
							this.zeroFlag
								.clear();
						}

						break;
					}
				case JNZ: //jump if not zero
					{
						//get the next byte
						this.nextByte();

						//determine whether the
						//zero flag is set
						if(this.zeroFlag 
							.getState()
							.getData()) {
							//flag is set
							this.nextByte();
							break;
						}

						//get a byte buffer to
						//get the address to
						//jump to
						ByteBuffer buf =
						ByteBuffer.allocate(2);

						//set the byte order
						//of the buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						//of the address
						byte b1 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte of
						//the address
						byte b2 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b2);

						//get the address from
						//the buffer
						char addr = buf.getChar(0);
						
						//set the program
						//counter to the address
						this.registers[0]
							.setValue(addr);

						//and set the jump
						//flag
						didJump = true;
						
						break;
					}
				case JZ: //jump if zero
					{
						//get the next byte
						this.nextByte();

						//determine whether the
						//zero flag is cleared
						if(!this.zeroFlag
							.getState()
							.getData()) {
							//flag is cleared
							this.nextByte();
							break;
						}

						//get a byte buffer to
						//get the address to
						//jump to
						ByteBuffer buf =
						ByteBuffer.allocate(2);

						//set the byte order
						//of the buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						//of the address
						byte b1 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte of
						//the address
						byte b2 = this.memory
								.peek(
							this.registers[0].
							getValue());

						//put it into the buffer
						buf.put(b2);

						//get the address from
						//the buffer
						char addr = buf.getChar(0);
						
						//set the program
						//counter to the address
						this.registers[0]
							.setValue(addr);

						//and set the jump
						//flag
						didJump = true;

						break;
					}
				case CALL: //subroutine call
					{
						//get the next byte
						this.nextByte();

						//create a ByteBuffer
						//to get the address of
						//the subroutine from
						ByteBuffer buf = 
						ByteBuffer.allocate(2);

						//set the byte order of
						//the buffer
						buf.order(ByteOrder.
							LITTLE_ENDIAN);

						//get the first byte
						//of the address
						byte b1 = this.memory
							.peek(
							this.registers[0].
							getValue());

						//put it in the buffer
						buf.put(b1);

						//get the next byte
						this.nextByte();

						//get the second byte of
						//the address
						byte b2 = this.memory
							.peek(
							this.registers[0].
							getValue());

						//put it in the buffer
						buf.put(b2);

						//get the address of the
						//subroutine
						char addr = buf.getChar(0);
						
						//push the program counter
						//onto the stack
						this.stack.push(
						this.registers[0]
							.getValue());

						//set the program
						//counter to the address
						//of the subroutine
						this.registers[0]
							.setValue(addr);

						//and set the jump
						//flag
						didJump = true;
							

						break;
					}
				case RET: //return from subroutine
					{
						//pop the top of the
						//stack into the program
						//counter
						this.registers[0]
							.setValue(
							this.stack.pop());

						break;
					}
				case SCMP: //compare strings
					{
						//get the next byte
						this.nextByte();

						//get the pointer
						//to the first string
						char p1 = this.registers[
							this.memory.peek(
							this.registers[0]
							.getValue())]
							.getValue();

						//get the next byte
						this.nextByte();

						//get the pointer to the
						//second string
						char p2 = this.registers[
							this.memory.peek(
							this.registers[0]
							.getValue())]
							.getValue();

						//get the first string
						String s1 = this.memory
							.retrieveString(
								p1);

						//get the second string
						String s2 = this.memory
							.retrieveString(
								p2);

						//compare the two
						//strings
						int cmp = s1.compareTo(s2);
						
						//and handle the result
						if(cmp == 0) { //equal
							//set the equals
							//flag
							this.equalsFlag
								.set();

							//and clear the
							//less-than
							//and greater-than
							//flags
							this.ltFlag
								.clear();
							this.gtFlag
								.clear();
						} else if(cmp < 0) {
							//set the less
							//than flag
							this.ltFlag.set();

							//and clear the
							//equals and
							//greater-than
							//flags
							this.equalsFlag
								.clear();
							this.gtFlag
								.clear();
						} else {
							//set the greater
							//than flag
							this.gtFlag.set();

							//and clear the
							//equals and
							//less-than 
							//flags
							this.equalsFlag
								.clear();
							this.ltFlag
								.clear();
						}

						break;
					}
				case SLEN: //get length of string
					{
						//get the next byte
						this.nextByte();

						//get a pointer to the
						//string
						char p = this.registers[
							this.memory.peek(
							this.registers[0]
							.getValue())]
							.getValue();

						//get the string
						//at the pointer's
						//address
						String s = this.memory
							.retrieveString(p);

						//get the length of the
						//string
						int len = s.length();

						//get the next byte
						this.nextByte();

						//get the register to
						//store the length in
						Register r = this
							.registers[
							this.memory.peek(
							this.registers[0]
							.getValue())];

						//store the length in the
						//register
						r.setValue((char)len);

						//and assign back
						//the updated register
						this.registers[
							r.getName()
							.getIndex()] = r;

						break;
					}
				case OREG: //output register
					{
						//get the next byte
						this.nextByte();

						//get the register to
						//output
						Register r =
							this.registers[
							this.memory.peek(
							this.registers[0]
							.getValue())];

						//and output its value
						System.out.println(
							(int)
							(r.getValue()));

						break;
					}
				case IREG: //input into register
					{
						//get the next byte
						this.nextByte();

						//get the register
						//to read into
						Register r =
							this.registers[
							this.memory.peek(
							this.registers[0]
							.getValue())];

						//read in a 16-bit value
						//from stdin
						int inVal = 0;
						if(this.sc.hasNextInt()) {
							//read in the int
							inVal = this.sc
							.nextInt();

							//consume the
							//newline
							this.sc.nextLine();
							
							//and put it in
							//the register
							r.setValue(
							(char)inVal);
						}

						//and assign back the
						//updated register
						this.registers[
						r.getName().getIndex()]
							= r;

						break;
					}
				case HCF: //halt and catch fire
					{
						while(true) {
							//loop until the
							//program is
							//killed
						}
					}
				case EXG: //exchange register values
					{
						//get the next byte
						this.nextByte();

						//get the first register
						Register r1 = 
							this.registers[
							this.memory.peek(
							this.registers[0]
							.getValue())];

						//get the next byte
						this.nextByte();

						//get the second register
						Register r2 =
							this.registers[
							this.memory.peek(
							this.registers[0]
							.getValue())];

						//exchange the values
						char tmp = r1.getValue();
						r1.setValue(r2.getValue());
						r2.setValue(tmp);

						//and assign back the
						//updated registers
						this.registers[
						r1.getName()
							.getIndex()] = r1;

						this.registers[
						r2.getName()
							.getIndex()] = r2;

						break;
					}
				default: //unknown instruction
					{
						throw new 
							ExecuteException();

					}

			}

			//get the next byte
			if(!didJump) {
				this.nextByte();
			}
		}
	}

	/**
	 * Increments the program counter to point to the next byte
	 */
	private void nextByte() {
		this.registers[0].inc(); //increment the program counter
	}

	/**
	 * Fetches the current opcode and returns it
	 *
	 * @return the current opcode in memory
	 */
	private Opcode fetch() {
		Opcode ret = Opcode.NOP; //the return value
		try {
			byte code = this.memory.peek(this.registers[0]
					.getValue());
			ret = Opcode.withCode(code);
		} catch(OpcodeException e) {
			//handle the exception
			e.printStackTrace();
		} catch(AccessException e) {
			//handle the exception
			e.printStackTrace();
		}

		//return the opcode
		return ret;
	}

	/**
	 * Closes the VM's assets
	 */
	public void close() {
		this.sc.close(); //close the Scanner
		this.memory.zero(); //zero the memory
		this.stack.clear(); //clear the stack
	}
	
}

//end of class
