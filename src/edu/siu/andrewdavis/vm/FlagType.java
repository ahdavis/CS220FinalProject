/*
 * FlagType.java
 * Enumerates types of flags for the VM
 * Created by Andrew Davis
 * Created on 4/13/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//enum definition
public enum FlagType {
	EQU, //equals flag
	ZRO, //zero flag
	LT, //less-than flag
	GT; //greater-than flag
}

//end of enum
