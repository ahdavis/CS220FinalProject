/*
 * RegName.java
 * Enumerates the names of different VM registers
 * Created by Andrew Davis
 * Created on 4/10/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//enum declaration
public enum RegName {
	PC(0x00, "PC"), //program counter
	A(0x01, "A"), //accumulator
	B(0x02, "B"), //general-purpose register
	C(0x03, "C"), //general-purpose register
	D(0x04, "D"), //general-purpose register
	E(0x05, "E"), //general-purpose register
	F(0x06, "F"); //general-purpose register

	//fields
	private int index; //the index of the register
	private String name; //the name of the register

	//constant
	public static final int REG_COUNT = 7; //the number of registers

	/**
	 * Constructor for the RegName enum
	 *
	 * @param newIndex the index of the register
	 * @param newName the name of the register
	 */
	private RegName(int newIndex, String newName) {
		this.index = newIndex; //init the index field
		this.name = newName; //init the name field
	}

	/**
	 * Returns a RegName instance corresponding to a name
	 *
	 * @param newName the name of the register
	 *
	 * @return the register corresponding to the name
	 *
	 * @throws RegNameException if the name does not match a register
	 */
	public static RegName withName(String newName) throws RegNameException {
		//loop and match the name
		for(int i = 0; i < RegName.values().length; i++) {
			if(RegName.values()[i].name.equals(newName)) { //name match
				return RegName.values()[i]; //return the register name
			}
		}

		//no match, so throw an exception
		throw new RegNameException(newName);
	}

	/**
	 * Returns the index of a given register
	 *
	 * @return the register's index
	 */
	public int getIndex() {
		return this.index; //return the index field
	}

	/**
	 * Converts the register name to a string
	 *
	 * @return the register name as a string
	 */
	@Override
	public String toString() {
		//switch on the register's index
		switch(this.index) {
			case 0: //program counter
				{
					return "PC";
				}
			case 1: //accumulator
				{
					return "A";
				}
			case 2: //general register B
				{
					return "B";
				}
			case 3: //general register C
				{
					return "C";
				}
			case 4: //general register D
				{
					return "D";
				}
			case 5: //general register E
				{
					return "E";
				}
			case 6: //general register F
				{
					return "F";
				}
			default: //unknown register
				{
					return "Unknown register";
				}
		}
	}
}

//end of enum
