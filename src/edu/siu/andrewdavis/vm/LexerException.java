/*
 * LexerException.java
 * Defines an exception that is thrown when a lexer encounters an error
 * Created by Andrew Davis
 * Created on 4/18/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//no imports

//class definition
public final class LexerException extends Exception
{
	/**
	 * First constructor for the LexerException class
	 *
	 * @param badChar the character that triggered the exception
	 */
	public LexerException(char badChar) {
		//call the superclass constructor
		super("Unknown character: " + badChar);
	}

	/**
	 * Second constructor for the LexerException class
	 *
	 * @param badChar the character that triggered the exception
	 * @param throwable a Throwable instance to pass to the Exception
	 */
	public LexerException(char badChar, Throwable throwable) {
		//call the superclass constructor
		super("Unknown character: " + badChar, throwable);
	}
}

//end of class
