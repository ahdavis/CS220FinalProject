/*
 * Register.java
 * Defines a class that represents a VM register
 * Created by Andrew Davis
 * Created on 4/10/2018
 * Copyright 2018 Andrew Davis
 * All rights reserved
 */

//package declaration
package edu.siu.andrewdavis.vm;

//imports
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

//class definition
public final class Register implements Comparable {
	//fields
	private RegName name; //the name of the register
	private char value; //the value of the register

	/**
	 * First constructor for the Register class
	 * Initializes the value of the Register to 0x0000
	 *
	 * @param newName the name of the Register
	 */
	public Register(RegName newName) {
		this(newName, (char)0x0000); //call the other constructor
	}

	/**
	 * Second constructor for the Register class
	 *
	 * @param newName the name of the Register
	 * @param newValue the value of the Register (in a short)
	 */
	public Register(RegName newName, char newValue) {
		this.name = newName; //init the name field
		this.value = newValue; //init the value field
	}

	/**
	 * Third constructor for the Register class
	 *
	 * @param newName the name of the Register
	 * @param newValue the value of the Register (in a byte)
	 */
	public Register(RegName newName, byte newValue) {
		this.name = newName; //init the name field
		this.setValue(newValue); //and set the byte value
	}

	//getter methods
	
	/**
	 * Returns the name of the register
	 *
	 * @return the name of the register
	 */
	public RegName getName() {
		return this.name; //return the name field
	}

	/**
	 * Returns the value of the register
	 *
	 * @return the value of the register
	 */
	public char getValue() {
		return this.value; //return the value field
	}

	//setter methods
	
	/**
	 * Stores a 16-bit value in the register
	 *
	 * @param newValue the value to store
	 */
	public void setValue(char newValue) {
		this.value = newValue; //set the value field
	}

	/**
	 * Stores a byte in the register
	 *
	 * @param newValue the byte to store
	 */
	public void setValue(byte newValue) {
		//allocate two bytes
		ByteBuffer buf = ByteBuffer.allocate(2);

		//make the buffer little-endian
		buf.order(ByteOrder.LITTLE_ENDIAN);

		//add the byte to the buffer
		buf.put(newValue);

		//and assign the buffer's value to the value field
		this.value = buf.getChar(0);
	}
		

	/**
	 * Increments the value of the register by 1
	 *
	 * @return the new value of the register
	 */
	public char inc() {
		this.value++; //increment the value field
		return this.value; //and return the incremented value
	}

	/**
	 * Decrements the value of the register by 1
	 *
	 * @return the new value of the register
	 */
	public char dec() {
		this.value--; //decrement the value field
		return this.value; //and return the decremented value
	}

	/**
	 * Adds a 16-bit value to the register's value
	 *
	 * @param num the value to add
	 *
	 * @return the new value of the register
	 */
	public char add(char num) {
		this.value += num; //add the number to the value field
		return this.value; //and return the value field
	}

	/**
	 * Subtracts a 16-bit value from the register's value
	 *
	 * @param num the value to subtract
	 *
	 * @return the new value of the register
	 */
	public char sub(char num) {
		this.value -= num; //subtract from the value field
		return this.value; //and return the value field
	}

	/**
	 * Multiplies a 16-bit value by the register's value
	 *
	 * @param num the value to multiply by
	 *
	 * @return the new value of the register
	 */
	public char mpy(char num) {
		this.value *= num; //add the number to the value field
		return this.value; //and return the value field
	}

	/**
	 * Divides the register's value by a 16-bit value 
	 *
	 * @param num the value to divide by
	 *
	 * @return the new value of the register
	 */
	public char div(char num) {
		this.value /= num; //add the number to the value field
		return this.value; //and return the value field
	}

	/**
	 * Calculates the register's value modulus a 16-bit value 
	 *
	 * @param num the value to modulate by
	 *
	 * @return the new value of the register
	 */
	public char mod(char num) {
		this.value %= num; //calculate the modulus
		return this.value; //and return the value field
	}

	/**
	 * Converts the register to a string
	 *
	 * @return the register as a string
	 */
	@Override
	public String toString() {
		return this.name.toString() + ": " + Integer.toString((int)this.value);
	}

	/**
	 * Compares two registers
	 *
	 * @param other the other register to compare
	 *
	 * @return 0 if the registers are equal, -1 if this register is smaller,
	 * or 1 if this register is greater
	 */
	public int compareTo(Object other) {
		//get Character objects from the two registers
		Character v1 = this.value;
		Register r2 = (Register)other;
		Character v2 = r2.value;

		//and return the comparison
		return v1.compareTo(v2);
	}

}

//end of class
